function initI18n() {
    i18next
        .use(i18nextBrowserLanguageDetector)
        .use(i18nextXHRBackend)
        .init({}, (err, t) => {
            if (err) console.error("something went wrong loading", err);
            jqueryI18next.init(i18next, $);
            localizePage();
        });
    
}

function initEventListeners(){
    $("#contact-form").on("submit", submitContactForm);
    $("#submit-button").on("click", () => $("#contact-form").submit());
    $(".copyright .lng").on("click", event => {
        event.preventDefault();
        const $anchor = $(event.currentTarget);
        const lng = $anchor.data("lng");
        i18next.changeLanguage(lng, (err, t) => {
            if (err) console.error("something went wrong switching language", err);
            localizePage();
        });
    });
}

function localizePage(){
    $("html").localize();
    $("input[type='text'], textarea").each((_, input)=>{
        const $input = $(input);
        const placeholderKey = $input.data('placeholderI18n');
        $input.attr("placeholder", i18next.t(placeholderKey));
    })
    $("input[type='submit']").each((_, input)=>{
        const $input = $(input);
        const valueKey = $input.data('valueI18n');
        $input.attr("value", i18next.t(valueKey));
    });
}

function getFormData($form){
    const unindexed_array = $form.serializeArray();
    const indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}

function validateLastSubmit(){
    const lastSubmit = localStorage.getItem('lastUpdate');
    if(!lastSubmit) return true;
    const lastSubmitDate = new Date(lastSubmit);
    return (new Date().getTime() - lastSubmitDate.getTime()) > 1 * 60 * 1000;
}

function validateForm(contactForm){
    return contactForm.name && contactForm.name.trim() !== ''
        && contactForm.email && contactForm.email.trim() !== ''
        && contactForm.message && contactForm.message.trim() !== '';
}

function submitContactForm(event){
    event.preventDefault();

    if(!validateLastSubmit()){
        Swal.fire({
            type: 'info',
            text: i18next.t('contact-error'),
          });
        return;
    }
    
    const request = getFormData($(event.currentTarget));

    if(!validateForm(request)){
        Swal.fire({
            type: 'info',
            text: i18next.t('fields-empty'),
          });
        return;
    }
    
    request.createdAt = new Date().toJSON();
    db.collection("contact-request").add(request)
    .then(()=> {
        $("#message").val("");
        $("#email").val("");
        $("#name").val("");
        Swal.fire({
            type: 'success',
            text: i18next.t('contact-sent')
        })
        localStorage.setItem('lastUpdate', request.createdAt);
    })
    .catch(()=>
        Swal.fire({
            type: 'error',
            text: i18next.t('contact-error'),
          }));
}

const spaceLaunch = () => {
    initI18n();
    initEventListeners();
    console.log(`🚀`)
};

module.exports = {
    spaceLaunch
}
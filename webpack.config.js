const webpack = require('webpack')
module.exports = {
    entry: {
      app: "./assets/js/app.js",
      main: "./assets/js/main.js" 
    },
    output: {
      filename: "[name].js",
      path: __dirname + '/assets/js/dist',
      libraryTarget: 'var',
      library: '[name]'
    },
  }